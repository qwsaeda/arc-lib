use std::env;
use std::fs;

use base64::{decode, encode};
use opencv::core::_InputArray;
use opencv::imgcodecs::IMREAD_COLOR;

use arc_lib::sdk;
use arc_lib::sdk::FaceResult;

fn main() {
    if let Err(msg) = demo() {
        println!("{}", msg);
    }
}

fn demo() -> FaceResult<()> {
    let app_id = env("APP_ID", "FnumvuHSGHnkiyeHpq9uhv574HkMcX2mroX73W4z2CHg");
    let sdk_key = env("SDK_KEY", "DudLoL4ATjvGNB2L59zj4rEFEPCGEwoJAMPnamfHL4Mw");
    println!("{}  {}", app_id.to_string(), sdk_key.to_string());
    sdk::check_arc_active(app_id.to_string(), sdk_key.to_string())?;
    let engine = sdk::ArcEngine_U::init_engine()?;
    let mut engine = sdk::ArcEngine::init_engine(&engine);
    let data = fs::read("d:/ht1.jpg").map_err(|e| format!("{}", e))?;
    // let face = sdk::get_img_info("d:/zk-2.jpg");
    let mut face = sdk::get_img_info_mem(data)?;

    let face_info = engine.face_check(&mut face)?;
    println!("face_info");
    let feature = engine.face_feature(&mut face, &face_info[0])?;
    println!("{}", encode(&feature.feature));
    engine.process(&mut face, &face_info)?;
    let age = engine.age()?;
    let gender = engine.gender()?;
    let angle = engine.angle()?;
    let live = engine.live()?;
    println!("age {},gender {},angle {},live {}", age, gender, angle, live);

    Ok(())
}

fn env<S: Into<String>>(key: S, default_value: S) -> Box<dyn ToString> {
    match env::var(key.into()) {
        Ok(value) => Box::new(value),
        Err(_) => Box::new(default_value.into()),
    }
}
