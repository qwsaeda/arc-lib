use arc_lib::sdk;

fn main() {
    println!("start");
    let version = sdk::arc_sdk_version();
    version.map(|v| {
        println!("Version:{}", v.version);
        println!("BuildDate:{}", v.build_date);
        println!("CopyRight:{}", v.copy_right);
    });
}