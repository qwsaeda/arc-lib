fn main() {
    let arrays = vec![0, 2, 5, 45, 4, 8, 845, 418548, 85, 848];
    let result = largest(&arrays);
    println!("{}", result);
    let arrays = vec!['a', 'b', 'c', 'z'];
    let result = largest(&arrays);
    println!("{}", result);
}

fn largest<T: PartialOrd>(list: &[T]) -> &T {
    let mut largest = &list[0];
    for item in list.into_iter() {
        if item > largest {
            largest = &item;
        }
    }
    largest
}