use std::env;
use std::path::PathBuf;

fn main() {
    // Tell cargo to tell rustc to link the system bzip2
    // shared library.
    let lib_path = if cfg!(target_os = "windows") {
        PathBuf::from(env::current_dir().unwrap().join("lib").join("X64"))
    } else {
        PathBuf::from("/face-lib")
    };
    println!("cargo:rerun-if-changed=wrapper.h");
    println!("cargo:rustc-link-search=all={}", lib_path.display());
    let lib_suffix = if cfg!(target_os = "windows") { "lib" } else { "" };
    println!("cargo:rustc-link-lib=dylib={}arcsoft_face_engine", lib_suffix);
    // println!("cargo:rustc-link-lib=dylib={}arcsoft_face",lib_suffix);
}